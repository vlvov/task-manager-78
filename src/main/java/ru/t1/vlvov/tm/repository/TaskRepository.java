package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    Task findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    List<Task> findByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId);

}
