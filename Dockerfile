FROM java:8

ADD ./target/tm-web.war .
EXPOSE 8080
EXPOSE 5701
ENTRYPOINT ["java", "-jar", "tm-web.war"]
